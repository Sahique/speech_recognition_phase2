from flask import Flask, request, render_template, jsonify
from flask_cors import CORS
import json
import os 
app = Flask(__name__)
CORS(app)
@app.route('/', methods= ['GET', 'POST'])
def get_message():
 # if request.method == "GET":
 print("Got request in main function")
 return render_template("index.html")
# this is the python application for dil studio
@app.route('/put_data',methods=['POST'])
def put_data():
    response={"ack": "success"}
    #try:
    data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
    print(">>",data)        #  s = data.decode()   >> code to decode byte string b'hello'
    j_req_data = json.loads(data)
    print(j_req_data)
    print(j_req_data['filename'],j_req_data['commandlist'])
    with open(j_req_data['filename']+'.json', 'w') as f:
      json.dump(j_req_data['commandlist'], f, indent=2)
      print("New json file is created from data.json file")
    return jsonify(response)

@app.route('/getData',methods=['POST'])
def get_data():

   # Opening JSON file
   data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
   print(">>")    
   print(data)        #  s = data.decode()   >>> code to decode byte string b'hello'
   j_req_data = json.loads(data)
   print(j_req_data['filename']),
   f = open(j_req_data['filename']+'.json')
   # returns JSON object as
   # a dictionary
   data = json.load(f)
   response={"ack": data}
   print (response)
   # Closing file
   f.close()
   return jsonify(response)




@app.route('/upload_static_file', methods=['POST'])
def upload_static_file():
   #f = request.files['static_file']
   #f.save(f.filename)
   fls=request.files.getlist('filelist')
   print("got fileupload message")
   print("list=",fls)
   for f in fls:    
      print ("fname = ",f.filename)
      f.save(f.filename)
   resp = {"success": True, "response": "file saved!"}
   return jsonify(resp), 200
@app.route('/listfiles', methods=['POST'])
def listfiles():
   basepath = './'
   flist=""
   first=0
   for entry in os.scandir(basepath):
      if entry.is_dir():    # skip directories
         continue
      else:
         if first==0:
            first=1
            flist=entry.name
         else:
            flist=flist+","+entry.name
   print("files=", flist) # use entry.path to get the full path of this entry, or use entry.name for the base filename
   return flist, 200

@app.route('/getfile', methods=['POST'])
def getfile():
   basepath = './'
   flist=""
   first=0
   fname=request.fname;
  
   return flist, 200

if __name__ == "__main__":
 app.run(host='0.0.0.0',port=5000, debug=True)



